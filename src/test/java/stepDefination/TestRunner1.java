package stepDefination;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/resources", glue= {"stepDefination"},plugin={"json:target/report/cucumber.json","html:target/report/report.html"})
public class TestRunner1  extends AbstractTestNGCucumberTests   {
	

}
