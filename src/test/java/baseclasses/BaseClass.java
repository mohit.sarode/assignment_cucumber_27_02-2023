package baseclasses;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseClass {

	static WebDriver driver;

	public static WebDriver getWebdriver()
	{

			driver= new ChromeDriver();

			driver.get("https://demowebshop.tricentis.com/");

			driver.manage().window().maximize();

			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			return driver;
		}
	
	}

