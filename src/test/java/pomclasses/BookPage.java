package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class BookPage {
	
	WebDriver driver;
	
	@FindBy(linkText="Books")
	private WebElement clickbooks;
	
	@FindBy(id="products-orderby")
	private WebElement selecthightolow;
	
	@FindBy(xpath="(//input[@value='Add to cart'])[1]")
	private WebElement addcartfirst;
	
	@FindBy(xpath="(//input[@value='Add to cart'])[2]")
	private WebElement addcartsecond;
	
	public BookPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	public void clickBooks()
	{
		clickbooks.click();
	}
	
	public void selectBook()
	{
		Select sl = new Select(selecthightolow);
		sl.selectByVisibleText("Price: High to Low");
	}
	
	public void addBook() throws InterruptedException
	{
		addcartfirst.click();
		Thread.sleep(2000);
		addcartsecond.click();
		
	}

}
