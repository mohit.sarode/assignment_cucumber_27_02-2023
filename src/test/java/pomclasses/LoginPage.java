package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;
	
	@FindBy(linkText="Log in")
	private WebElement clicklogin;
	
	@FindBy(id="Email")
	private WebElement enteremail;
	
	@FindBy(id="Password")
	private WebElement enterpassword;
	
	@FindBy(xpath="//input[@value='Log in']")
	private WebElement submitlogin;
	
	public LoginPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	public void clickLogin()
	{
		clicklogin.click();
	}
	
	public void enterEmail(String email)
	{
		enteremail.sendKeys(email);
	}
	
	public void enterPassword(String password)
	{
		enterpassword.sendKeys(password);
	}
	
	public void submitLogin()
	{
		submitlogin.click();
	}

}
